# Deposit Slip

Print a deposit slip with your account number and checking option selected.

Currently, this works for `Rev. 8/23` deposit slips.

## Printing 🖨️

Currently, I'm only able to get printing to work on Windows.

- Select `Print using the system dialog...`.
- Select `More settings`.
- For `Printer Paper Size` select `Custom...`.
  - Units: `inch`
  - Width: `3.375`
  - Height: `7.5`
- For `Orientation` select `Landscape`.
- 🤞
